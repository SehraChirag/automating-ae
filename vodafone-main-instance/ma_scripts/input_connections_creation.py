import time
import sys
import os
import pandas as pd
from contextlib import contextmanager
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import staleness_of

class InputConnection:
	URL_ADDRESS = "http://vodafone-main.vodafone.cardinality.pl"
	CONNECTION_TYPE = "hdfs"
	OUTPUT_IP_ADDRESS = "10.0.0.30"
	OUTPUT_IP_PORT = "8020"

	def __init__(self, country, vendor, tech):
		self.country = country
		self.vendor = vendor
		self.tech = tech
		self.configFileName = country + "_" + vendor + "_" + tech + ".xlsx"
		self.driver = webdriver.Chrome()

	@contextmanager
	def waitForPageLoad(self, timeout=30):
		old_page = self.driver.find_element_by_tag_name('html')
		yield
		WebDriverWait(self.driver, timeout).until(
			staleness_of(old_page)
		)

	def doConnection(self):
		try:
			# Reading the config file
			# df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)
			path = os.getcwd()+"/data/" + self.country + "/" + self.vendor + "/" + self.tech + "/" + self.configFileName
			df = pd.read_excel(path)

			df = df.drop_duplicates(subset='Connection Name(Input HDFS)', keep="first")
			
			driver =  self.driver
			driver.get(self.URL_ADDRESS)

			with self.waitForPageLoad(timeout=5):
				username_elem = driver.find_element_by_id('username')
				password_elem = driver.find_element_by_id('password')
				username_elem.send_keys('admin')
				password_elem.send_keys('wkswks12')

				sign_in = driver.find_element_by_tag_name('button')
				sign_in.click()
				time.sleep(2)
				driver.get(self.URL_ADDRESS + "/analytics/connections/new")
				
			for index, row in df.iterrows():
				with self.waitForPageLoad(timeout=5):
					connectionName = row['Connection Name(Input HDFS)']
					connectionPath = row['Input Connection HDFS Path']

					print("Creating connection for ", connectionName)
					print("@index: ", index)
					
					driver.find_element_by_id('name').send_keys(connectionName)

					#Setting the connection type to 'HDFS'
					driver.find_element_by_css_selector('div#type input').click()
					conType_elem = driver.find_element_by_css_selector('.MuiPaper-root.MuiPaper-elevation8 input')
					ActionChains(driver).move_to_element(conType_elem).click().send_keys("HDFS").send_keys(Keys.ENTER).perform()
					time.sleep(0.5)
				
					driver.find_element_by_id('server').send_keys('10.0.0.30')
					driver.find_element_by_id('port').send_keys('8020')
					driver.find_element_by_id('path').send_keys(connectionPath)

					# Setting additional config
					additionalConfigs = ["file-extension", "separator"]
					for conf in additionalConfigs:
						driver.find_element_by_id('addConfig1').click()
						configList_elem = driver.find_element_by_css_selector(".MuiPaper-root.MuiPaper-elevation8 input")
						ActionChains(driver).move_to_element(configList_elem).click().send_keys(conf).send_keys(Keys.ENTER).perform()
						time.sleep(0.5)

					driver.find_element_by_xpath("//form[1]").submit()
					time.sleep(2)
					print("Connection: ", connectionName, " has been created.")
					driver.get(self.URL_ADDRESS + "/analytics/connections/new")
				
			time.sleep(10)
			driver.close()
		except IndexError as ie:
			# Catching error with insufficient no of parameters defined.
			print("Error is ", ie)
			print("Usage: python ", sys.argv[0], " $projectPath $country $vendor $tech")

def main():
	try:
		print(os.getcwd())
		country = sys.argv[1]
		vendor = sys.argv[2]
		tech = sys.argv[3]

		v = InputConnection(country, vendor, tech)
		v.doConnection()
	except IndexError as ie:
		print("Error is ", ie, ". Please see the usage syntax below")
		print("Usage: python ", sys.argv[0], " $country $vendor $tech")

if __name__ == "__main__":
	main()