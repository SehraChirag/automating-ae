import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

import pyperclip
import logging
import os

COUNTRY = "VFIT"
VENDOR = "Huawei"
TECH = "2G"
config_filename = "VFIT_Huawei_2G.xlsx"

URL_ADDRESS = "http://vodafone-main.vodafone.cardinality.pl/analytics/"

path_to_logs = "data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + "logs/"
logs_filename = "q2_event_logs_" + COUNTRY + "_" + VENDOR + "_" + TECH + ".csv"

if not os.path.exists(path_to_logs):
	os.makedirs(path_to_logs)

with open(path_to_logs + logs_filename, 'w') as fp:
	pass

logging.basicConfig(level=logging.INFO, filename=path_to_logs + logs_filename)


def main(project_id):
	url = URL_ADDRESS+'projects/' + project_id + '/queries'
	q_dir = 'data/'+COUNTRY+"/"+VENDOR+"/"+TECH+"/"+'q2/'

	# Reading the config file
	df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)
	logging.info("Working on instance:{0} with project_id: {1}".format(url, project_id))
	driver = webdriver.Chrome()
	# print(df.shape)
	df = df.drop_duplicates(subset='QueryName with Output', keep='first')
	print("Total number of rows remaining after removing duplicates are {}".format(df.shape[0]))

	driver.get(url)
	username_elem = driver.find_element_by_id('username')
	password_elem = driver.find_element_by_id('password')
	username_elem.send_keys('admin')
	password_elem.send_keys('wkswks12')
	driver.find_element_by_tag_name('button').click()
	time.sleep(3)
	driver.get(url)
	time.sleep(3)
	# time.sleep(5)
	df = df.reset_index(drop=True)

	# if your code stops working in between, look at the terminal. What ever is the last index put it here and run again
	# last_index = 135
	# df = df[last_index:]

	for index, row in df.iterrows():
		query_name = row['QueryName with Output']
		print("Index: ", index)
		print("Query Name is {0}".format(query_name))
		begin_time = time.time()
		try:
			driver.get(URL_ADDRESS+'projects/' + project_id + '/queries/new')
			time.sleep(2)
			driver.find_element_by_id('name').send_keys(query_name)
			driver.find_element_by_id('isCustomSql').click()
			time.sleep(1)
			custom_query = driver.find_element_by_id('customSql').find_element_by_tag_name('textarea')
			with open(q_dir + query_name + ".txt") as f:
				text = f.read()
			# print(text.strip())
			pyperclip.copy(text)
			time.sleep(4)
			custom_query.send_keys(Keys.CONTROL, 'v')
			# custom_query.send_keys(text)
			time.sleep(2)
			driver.find_element_by_id("outputConnection").click()
			time.sleep(1)
			connection_name = driver.find_element_by_class_name('jss264').find_element_by_tag_name('input')
			time.sleep(1)
			connection_name.send_keys(row['Connection Name(Output-Hive)'])
			time.sleep(1)
			connection_select = driver.find_element_by_xpath("/html/body/div[3]/div[3]/div[1]/following-sibling::div")
			items = connection_select.find_elements_by_class_name("MuiMenuItem-gutters")
			for i in items:
				if row['Connection Name(Output-Hive)'].strip() == i.text.split("\n")[0].strip():
					time.sleep(2)
					i.click()
					break
			time.sleep(2)
			driver.find_element_by_class_name('jss180').click()
			time.sleep(2)
			print("Total time taken by {0} is {1}".format(query_name, time.time() - begin_time))
			logging.info(index, query_name, begin_time - time.time(), "Successfully Created")
		except Exception as e:
			print(str(e))
			logging.error(index, query_name, begin_time - time.time(), str(e))
	driver.close()


main("5f22a9bf18df3b162def3060")
