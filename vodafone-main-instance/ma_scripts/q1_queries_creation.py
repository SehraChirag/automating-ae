import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import time
import pyperclip
import logging

COUNTRY = "VFIT"
VENDOR = "Huawei"
TECH = "2G"
config_filename = "VFIT_Huawei_2G.xlsx"

URL_ADDRESS = "http://vodafone-main.vodafone.cardinality.pl/analytics/"

path_to_logs = "data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + "logs/"
logs_filename = "q1_event_logs_" + COUNTRY + "_" + VENDOR + "_" + TECH + ".csv"

if not os.path.exists(path_to_logs):
	os.makedirs(path_to_logs)

with open(path_to_logs + logs_filename, 'w') as fp:
	pass

logging.basicConfig(level=logging.INFO, filename=path_to_logs + logs_filename)


def main(project_id):
	url = URL_ADDRESS+'projects/' + project_id + '/queries'
	q_dir = 'data/'+COUNTRY+"/"+VENDOR+"/"+TECH+"/"+'q1/'

	# Reading the config file
	df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)

	df = df.drop_duplicates(subset='QueryName to Extract VFUID', keep='first')
	driver = webdriver.Chrome()
	print("Total number of rows remaining after removing duplicates are {}".format(df.shape[0]))
	driver.get(url)

	username_elem = driver.find_element_by_id('username')
	password_elem = driver.find_element_by_id('password')
	username_elem.send_keys('admin')
	password_elem.send_keys('wkswks12')
	driver.find_element_by_tag_name('button').click()
	time.sleep(2)
	driver.get(url)
	time.sleep(2)
	df = df.reset_index(drop=True)

	# if your code stops working in between, look at the terminal. What ever is the last index put it here and run again
	# last_index = 135
	# df = df[last_index:]

	for index, row in df.iterrows():
		query_name = row['QueryName to Extract VFUID']
		print("Index: ", index)
		print("Query Name is {0}".format(query_name))
		begin_time = time.time()
		# print(URL_ADDRESS+'/projects/' + project_id + '/queries/new')
		try:
			driver.get(URL_ADDRESS+'projects/' + project_id + '/queries/new')

			time.sleep(2)
			driver.find_element_by_id('name').send_keys(query_name)
			driver.find_element_by_id('isCustomSql').click()
			time.sleep(1)
			custom_query = driver.find_element_by_id('customSql').find_element_by_tag_name('textarea')
			# print(custom_query)
			with open(q_dir + query_name + ".txt") as f:
				text = f.read()
			pyperclip.copy(text)
			time.sleep(4)
			custom_query.send_keys(Keys.CONTROL, 'v')
			# custom_query.send_keys(text)
			time.sleep(1)
			# time.sleep(3)
			driver.find_element_by_class_name('jss180').click()
			print("Total time taken by {0} is {1}".format(query_name, time.time() - begin_time))
			logging.info(index, query_name, begin_time - time.time(), "Successfully Created")
		except Exception as e:
			print(str(e))
			logging.info(index, query_name, begin_time - time.time(), str(e))
	driver.close()


main(project_id="5f1efd1f18df3b162def2f5c")
