import time
import sys
import os
import pandas as pd
from contextlib import contextmanager
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import staleness_of


class Project:
    URL_ADDRESS = "http://vodafone-main.vodafone.cardinality.pl"
    CONNECTION_TYPE = "hdfs"
    OUTPUT_IP_ADDRESS = "10.0.0.30"
    OUTPUT_IP_PORT = "8020"

    def __init__(self, country, vendor, tech):
        self.country = country
        self.vendor = vendor
        self.tech = tech
        self.configFileName = country + "_" + vendor + "_" + tech + ".xlsx"
        self.driver = webdriver.Chrome()

    @contextmanager
    def waitForPageLoad(self, timeout=30):
        old_page = self.driver.find_element_by_tag_name('html')
        yield
        WebDriverWait(self.driver, timeout).until(
            staleness_of(old_page)
        )

    def doProject(self):
        try:
            # Reading the config file
            # df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)
            path = os.getcwd()+"/data/" + self.country + "/" + self.vendor + \
                "/" + self.tech + "/" + self.configFileName
            df = pd.read_excel(path)

            df = df.drop_duplicates(subset='Model Name', keep="first")

            driver = self.driver
            driver.get(self.URL_ADDRESS)

            with self.waitForPageLoad(timeout=5):
                username_elem = driver.find_element_by_id('username')
                password_elem = driver.find_element_by_id('password')
                username_elem.send_keys('admin')
                password_elem.send_keys('wkswks12')

                sign_in = driver.find_element_by_tag_name('button')
                sign_in.click()
                time.sleep(2)
                driver.get(self.URL_ADDRESS +
                           "/analytics/projects/new/settings")

            projectName = self.country.upper() + " " + self.vendor.title() + " " + self.tech
            driver.find_element_by_id('name').send_keys(projectName)
            
            for index, row in df.iterrows():
                modelName = row['Model Name']

                print("Mapping model: {0} at Index {1}", modelName, index)
                driver.find_element_by_css_selector('.MuiInputBase-root.MuiInput-root.jss203.jss276 input').send_keys(modelName)
                time.sleep(3)
                
                button_elem = driver.find_element_by_xpath('//div[@draggable="true"]/div/div/button')
                ActionChains(driver).move_to_element(button_elem).click(button_elem).perform()
                    
            driver.find_element_by_xpath("//form[1]").submit()
            time.sleep(10)
            driver.close()
        except IndexError as ie:
            # Catching error with insufficient no of parameters defined.
            print("Error is ", ie)
            print("Usage: python ", sys.argv[0],
                  " $projectPath $country $vendor $tech")


def main():
    try:
        print(os.getcwd())
        country = sys.argv[1]
        vendor = sys.argv[2]
        tech = sys.argv[3]

        v = Project(country, vendor, tech)
        v.doProject()
    except IndexError as ie:
        print("Error is ", ie, ". Please see the usage syntax below")
        print("Usage: python ", sys.argv[0], " $country $vendor $tech")


if __name__ == "__main__":
    main()
