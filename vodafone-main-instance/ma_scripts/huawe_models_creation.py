import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
import pyperclip
import logging

driver = webdriver.Chrome()

COUNTRY = "VFIT"
VENDOR = "Huawei"
TECH = "2G"
config_filename = "VFIT_Huawei_2G.xlsx"

URL_ADDRESS = "http://vodafone-main.vodafone.cardinality.pl/analytics/"

path_to_logs = "data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + "logs/"
logs_filename = "models_" + COUNTRY + "_" + VENDOR + "_" + TECH + ".csv"

if not os.path.exists(path_to_logs):
    os.makedirs(path_to_logs)

with open(path_to_logs+logs_filename, 'w') as fp:
    pass

logging.basicConfig(level=logging.INFO, filename=path_to_logs+logs_filename)


def main():
    f_dir = 'data/'+COUNTRY+"/"+VENDOR+"/"+TECH+"/"+'models/'
    model_files = os.listdir(f_dir)
    # print(model_files)

    # Reading the config file
    df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)

    df = df[df['country'] == COUNTRY.lower()]
    df = df.drop_duplicates(subset=['measinfo', 'mt_hash', 'element'], keep="first")
    df['Model Name'] = df['Model Name'].str.replace('_', '')
    df = df.reset_index(drop=True)
    # print(df)
    driver.get(URL_ADDRESS+"models")
    username_elem = driver.find_element_by_id('username')
    password_elem = driver.find_element_by_id('password')
    username_elem.send_keys('admin')
    password_elem.send_keys('wkswks12')
    driver.find_element_by_tag_name('button').click()
    time.sleep(5)

    # if your code stops working in between, look at the terminal. What ever is the last index put it here and run again
    # last_index = 135
    # df = df[last_index:]

    driver.get(URL_ADDRESS+"models")
    for index, row in df.iterrows():
        print("Index: ", index)
        print("Model Name is {0} and Input Connection Name is {1}: ".format(row['Model Name'], row['Connection Name('
                                                                                                   'Input HDFS)']))
        model_name = row['Model Name']

        driver.get(URL_ADDRESS+'models/new')
        driver.find_element_by_id('name').send_keys(model_name)
        driver.find_element_by_id('inputConnection').click()
        time.sleep(1)
        select_connection = driver.find_element_by_class_name('jss263').find_element_by_tag_name('input')
        select_connection.send_keys(row['Connection Name(Input HDFS)'])
        time.sleep(3)
        items = driver.find_elements_by_class_name('MuiMenuItem-gutters')
        for item in items:
            if row['Connection Name(Input HDFS)'] + '\n' in item.text:
                item.click()
                time.sleep(2)
                break
        # time.sleep(3)
        driver.find_element_by_class_name('jss280').click()

        driver.find_element_by_id('customTextMode').click()
        driver.find_element_by_id('containsHeader').click()
        # model_file = list(filter(lambda x: x.split('_')[-1][:-4] == row['mt_hash'][:8], model_files))[0]
        model_file = list(
            filter(lambda x: x.rsplit("_")[-2].lower() + x.rsplit("_")[-1].rsplit(".")[0] ==
                              row["element"].lower() + row["mt_hash"][:8],
                   model_files))[0]
        print("The Appropriate Model File is {}".format(model_file))
        with open(f_dir + model_file, 'r') as f:
            text = f.readline()
        print("Models headers are {0}".format(text))
        pyperclip.copy(text.replace('\n', ''))
        time.sleep(5)
        driver.find_element_by_id('customText').send_keys(Keys.CONTROL, 'v')
        buttons = driver.find_elements_by_class_name('jss193')
        logging.info(index, row['Model Name'], "Successfully Created")
        save_button_1 = buttons[2]
        save_button_2 = buttons[0]
        time.sleep(1)
        save_button_1.click()
        time.sleep(4)
        save_button_2.click()
        time.sleep(2)

    driver.close()


main()

