import pandas as pd
from selenium import webdriver
import time
import pyperclip
from selenium.webdriver.common.keys import Keys
import os
import logging

debug = False

driver = webdriver.Chrome()

COUNTRY = "VFHU"
VENDOR = "Huawei"
TECH = "2G"
config_filename = "VFHU_Huawei_2G.xlsx"

URL_ADDRESS = "http://10.0.0.146:31081/analytics/"
CONNECTION_TYPE = "hdfs"
OUTPUT_IP_ADDRESS = "10.0.0.30"
OUTPUT_IP_PORT = "8020"

path_to_logs = "data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + "logs/"
logs_filename = "input_connections_" + COUNTRY + "_" + VENDOR + "_" + TECH + ".csv"

if not os.path.exists(path_to_logs):
    os.makedirs(path_to_logs)

with open(path_to_logs + logs_filename, 'w') as fp:
    pass

logging.basicConfig(level=logging.INFO, filename=path_to_logs + logs_filename)


def main():
    # Reading the config file
    df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)

    df = df.drop_duplicates(subset='Connection Name(Input HDFS)', keep="first")
    # print(df.shape)

    # Logging into AE
    driver.get(URL_ADDRESS+'connections/new')
    username_elem = driver.find_element_by_id('username')
    password_elem = driver.find_element_by_id('password')
    username_elem.send_keys('admin')
    password_elem.send_keys('wkswks12')
    driver.find_element_by_tag_name('button').click()
    time.sleep(5)
    df = df.reset_index(drop=True)

    # if your code stops working in between, look at the terminal. What ever is the last index put it here and run again
    # last_index = 135
    # df = df[last_index:]
    for index, row in df.iterrows():
        print("Index: ", index)
        print("Connection Name: ", row['Connection Name(Input HDFS)'])
        connection_name = row['Connection Name(Input HDFS)']
        driver.get('http://10.0.0.146:31081/analytics/connections/new')
        driver.find_element_by_id('name').send_keys(connection_name)
        driver.find_element_by_id('type').click()
        time.sleep(3)
        items = driver.find_elements_by_class_name('MuiMenuItem-gutters')
        for item in items:
            # print(item.text)
            if item.text.lower() == 'hdfs':
                item.click()
                time.sleep(1)
                break
        driver.find_element_by_id('server').send_keys('10.0.0.30')
        driver.find_element_by_id('port').send_keys('8020')
        pyperclip.copy(row['Input Connection HDFS Path'])
        driver.find_element_by_id('path').send_keys(Keys.CONTROL, 'v')
        # time.sleep(2)
        for i in ['file-extension', 'separator']:
            driver.find_element_by_id('addConfig1').click()
            time.sleep(1)
            items = driver.find_elements_by_class_name('MuiMenuItem-gutters')
            for item in items:
                # print(item.text)
                if item.text.lower() == i:
                    item.click()
                    time.sleep(1)
                    break
        time.sleep(1)
        if not debug:
            driver.find_element_by_class_name('jss196').click()
            logging.info(index, row['Connection Name(Input HDFS)'], "Successfully Created")
        time.sleep(1)
    driver.close()


main()
