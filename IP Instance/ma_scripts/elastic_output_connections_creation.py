import time

import pandas as pd
from selenium import webdriver
import logging
import os


# Set to FALSE, if changing some code
debug = True

driver = webdriver.Chrome()

COUNTRY = "VFHU"
VENDOR = "Huawei"
TECH = "2G"
config_filename = "VFHU_Huawei_2G.xlsx"

URL_ADDRESS = "http://10.0.0.146:31081/analytics/"
CONNECTION_TYPE = "elastic"
OUTPUT_IP_ADDRESS = "10.0.0.122"
OUTPUT_IP_PORT = "31921"

path_to_logs = "data/"+ COUNTRY + "/" + VENDOR + "/" + TECH + "/"+"logs/"
logs_filename = "elastic_output_connections_" + COUNTRY + "_"+VENDOR+"_"+TECH+".csv"

if not os.path.exists(path_to_logs):
	os.makedirs(path_to_logs)

with open(path_to_logs+logs_filename,'w') as fp:
	pass

logging.basicConfig(level=logging.INFO, filename=path_to_logs+logs_filename)


def main():
	# Reading the config file
	df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)

	# Logging into AE
	driver.get(URL_ADDRESS + "connections")
	username_elem = driver.find_element_by_id('username')
	password_elem = driver.find_element_by_id('password')
	username_elem.send_keys('admin')
	password_elem.send_keys('wkswks12')
	driver.find_element_by_tag_name('button').click()
	time.sleep(5)

	# if your code stops working in between, look at the terminal. What ever is the last index put it here and run again
	# last_index = 135
	# df = df[last_index:]
	for index, row in df.iterrows():
		print("Index: ",index)
		print("Connection Name: ",row["Connection Name(Output-Elastic)"])

		for connection_type, host, port in [(CONNECTION_TYPE, OUTPUT_IP_ADDRESS, OUTPUT_IP_PORT)]:
			connection_name = row["Connection Name(Output-Elastic)"]
			driver.get(URL_ADDRESS+'connections/new')
			time.sleep(2)
			driver.find_element_by_id('name').send_keys(connection_name)
			driver.find_element_by_id('type').click()
			time.sleep(2)
			items = driver.find_elements_by_class_name('MuiMenuItem-gutters')
			# print(items)
			for item in items:
				if item.text.lower() == "elasticsearch":
					item.click()
					time.sleep(2)
					break
			driver.find_element_by_id('server').send_keys(host)
			driver.find_element_by_id('port').send_keys(port)

			elastic_index = row["Elastic Index"]
			driver.find_element_by_id('index').send_keys(elastic_index)

			for i in ['schema']:
				driver.find_element_by_id('addConfig1').click()
				time.sleep(1)
				items = driver.find_elements_by_class_name('MuiMenuItem-gutters')
				for item in items:
					# print(item.text)
					if item.text.lower() == i:
						item.click()
						time.sleep(1)
						break
			time.sleep(1)
			if not debug:
				driver.find_element_by_class_name('jss196').click()
				logging.info(index,row["Connection Name(Output-Elastic)"],"Successfully Created")
			time.sleep(1)
	driver.close()


main()
