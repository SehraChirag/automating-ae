import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
import pyperclip
import logging

driver = webdriver.Chrome("/home/valiance/Downloads/chromedriver")
COUNTRY = "vfde"
VENDOR = "nokia"
FAMILY = "r4"
NE_TYPE = "omgw_omgw"
config_filename = COUNTRY + "_" + VENDOR + "_" + FAMILY + "_" + NE_TYPE + ".xlsx"

URL_ADDRESS = "http://10.0.0.146:31081/analytics/"


def main():
    path = os.getcwd()
    f_dir = path + '/data/' + COUNTRY + "/" + VENDOR + "/" + FAMILY + "/" + NE_TYPE + "/" + 'models/'
    model_files = os.listdir(f_dir)
    print(model_files)

    # Reading the config file
    df = pd.read_excel(path + "/data/" + COUNTRY + "/" + VENDOR + "/" + FAMILY + "/" + NE_TYPE + "/" + config_filename)

    df = df[df['country'] == COUNTRY.lower()]
    df = df.drop_duplicates(subset=['measinfo', 'mt_hash'], keep="first")
    # df['Model Name'] = df['Model Name'].str.replace('_', '')
    # df['measinfo'] = df['measinfo'].str.replace('_', '')
    df = df.reset_index(drop=True)
    driver.get(URL_ADDRESS + "models")
    username_elem = driver.find_element_by_id('username')
    password_elem = driver.find_element_by_id('password')
    username_elem.send_keys('admin')
    password_elem.send_keys('wkswks12')
    driver.find_element_by_tag_name('button').click()
    time.sleep(5)

    # if your code stops working in between, look at the terminal. What ever is the last index put it here and run again
    last_index = 21
    df = df[last_index:]

    driver.get(URL_ADDRESS + "models")
    for index, row in df.iterrows():
        print("Index: ", index)
        print("Model Name is {0} and Input Connection Name is {1}: ".format(row['Model Name'], row['Connection Name('
                                                                                                   'Input HDFS)']))
        model_name = row['Model Name']

        driver.get(URL_ADDRESS + 'models/new')
        driver.find_element_by_id('name').send_keys(model_name)
        driver.find_element_by_id('inputConnection').click()
        time.sleep(1)
        select_connection = driver.find_element_by_class_name('jss263').find_element_by_tag_name('input')
        select_connection.send_keys(row['Connection Name(Input HDFS)'])
        time.sleep(3)
        items = driver.find_elements_by_class_name('MuiMenuItem-gutters')
        for item in items:

            if row['Connection Name(Input HDFS)'] + '\n' in item.text:
                item.click()
                time.sleep(2)
                break
        # time.sleep(3)
        driver.find_element_by_class_name('jss280').click()

        driver.find_element_by_id('customTextMode').click()
        driver.find_element_by_id('containsHeader').click()
        # model_file = list(filter(lambda x: x.split('_')[-1][:-4] == row['mt_hash'][:8], model_files))[0]
        model_file = list(
            filter(lambda i: "vc" + "".join(
                [j.lower()[:3] if j.lower() == "nokia" else j.lower() for j in i.split("_")]) == row[
                                 "Model Name"].lower() + ".csv", model_files))[0]
        print("The Appropriate Model File is {}".format(model_file))
        with open(f_dir + model_file, 'r') as f:
            text = f.readline()
        # print(text)
        print("Models headers are {0}".format(text))
        pyperclip.copy(text.replace('\n', ''))
        time.sleep(3)
        driver.find_element_by_id('customText').send_keys(Keys.CONTROL, 'v')
        buttons = driver.find_elements_by_class_name('jss193')
        logging.info(index, row['Model Name'], "Successfully Created")
        save_button_1 = buttons[2]
        save_button_2 = buttons[0]
        time.sleep(1)
        save_button_1.click()
        time.sleep(2)
        save_button_2.click()
        time.sleep(2)

    driver.close()


main()
