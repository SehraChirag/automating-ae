import pandas as pd
from selenium import webdriver
import time
import pyperclip
from selenium.webdriver.common.keys import Keys
import os
from webdriver_manager.chrome import ChromeDriverManager

debug = False

driver = webdriver.Chrome(ChromeDriverManager().install())

COUNTRY = "VFHU"
VENDOR = "Huawei"
TECH = "2G"
config_filename = "VFHU_Huawei_2G.xlsx"

URL_ADDRESS = "http://vodafone-michal.vodafone.cardinality.pl/analytics/"
CONNECTION_TYPE = "hdfs"
OUTPUT_IP_ADDRESS = "10.0.0.30"
OUTPUT_IP_PORT = "8020"


def main():
	# reading the config file
	df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)

	# print(df.shape)

	df = df.drop_duplicates(subset='project_name', keep="first")
	print(df.shape)

	driver.get(URL_ADDRESS)
	username_elem = driver.find_element_by_id('username')
	password_elem = driver.find_element_by_id('password')
	username_elem.send_keys('admin')
	password_elem.send_keys('wkswks12')

	driver.find_element_by_tag_name('button').click()
	time.sleep(5)
	df = df.reset_index(drop=True)

	# if your code stops working in between, look at the terminal. What ever is the last index put it here and run again
	# last_index = 135
	# df = df[last_index:]

	for index, row in df.iterrows():
		print("Index: {0} \t Project Name: {1}".format(index, row['project_name']))
		time.sleep(3)

		# search bar component
		driver.find_element_by_css_selector('.MuiInputBase-root.MuiInput-root.jss272.jss278 input').send_keys(row['project_name'])

		driver.find_element_by_id('btn-project-create').click()
		time.sleep(3)
		driver.find_element_by_id("name").send_keys(row['project_name'])
		time.sleep(1)
		driver.find_element_by_css_selector(".MuiButtonBase-root.jss321.jss642.jss326.btn-submit").click()


main()
