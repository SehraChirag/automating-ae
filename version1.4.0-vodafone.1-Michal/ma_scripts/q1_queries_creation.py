import pandas as pd
from selenium import webdriver
import time
import pyperclip
from selenium.webdriver.common.keys import Keys
import os
# import Action chains
from selenium.webdriver.common.action_chains import ActionChains
from webdriver_manager.chrome import ChromeDriverManager

debug = False

driver = webdriver.Chrome(ChromeDriverManager().install())

COUNTRY = "VFHU"
VENDOR = "Huawei"
TECH = "2G"
config_filename = "VFHU_Huawei_2G.xlsx"

URL_ADDRESS = "http://vodafone-michal.vodafone.cardinality.pl/analytics/"


def main():
	# reading the config file
	q_dir = 'data/' + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + 'q1/'
	project_url_mapping = dict()
	df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)
	data = df
	df = df.drop_duplicates(subset='QueryName to Extract VFUID', keep="first")
	data = data.drop_duplicates(subset='project_name', keep='first')

	driver.get(URL_ADDRESS)
	username_elem = driver.find_element_by_id('username')
	password_elem = driver.find_element_by_id('password')
	username_elem.send_keys('admin')
	password_elem.send_keys('wkswks12')

	driver.find_element_by_tag_name('button').click()
	time.sleep(5)
	df = df.reset_index(drop=True)
	data = data.reset_index(drop=True)

	for index, row in data.iterrows():
		project_name = row['project_name']
		# print(project_name)
		searchbar = driver.find_element_by_css_selector('.MuiInputBase-root.MuiInput-root.jss272.jss278 input')
		searchbar.send_keys(
			project_name)
		time.sleep(1)
		address_link = driver.find_element_by_css_selector(".MuiGrid-root.MuiGrid-item.MuiGrid-grid-md-3 a").get_attribute("href")
		project_url_mapping[row['project_name']] = address_link
		searchbar.click()
		searchbar.send_keys(Keys.CONTROL, 'a')
		searchbar.send_keys(Keys.DELETE)
	# print(project_url_mapping)

	for index, row in df.iterrows():
		print("Index: {0} \t Query Name: {1}".format(index, row['QueryName to Extract VFUID']))
		query_name = row['QueryName to Extract VFUID']
		project_name = row['project_name']

		URL = project_url_mapping.get(project_name)
		# print(URL)
		driver.get(URL)
		time.sleep(2)
		# create button
		create = driver.find_element_by_id("1").click()
		time.sleep(1)
		menu_list = driver.find_elements_by_tag_name("li")
		# print(menu_list)
		for item in menu_list:
			# print(items.text)
			if item.text == 'Query':
				item.click()
				time.sleep(1)
				break
		time.sleep(3)
		driver.find_element_by_id('name').send_keys(query_name)
		driver.find_element_by_id('isCustomSql').click()
		time.sleep(1)
		custom_query = driver.find_element_by_id('customSql').find_element_by_tag_name('textarea')
		with open(q_dir + query_name + ".txt") as f:
			text = f.read()
		time.sleep(2)
		pyperclip.copy(text.replace('\n', ''))
		# custom_query.send_keys(Keys.CONTROL, 'a')
		# custom_query.send_keys(Keys.DELETE)
		custom_query.send_keys(Keys.CONTROL, 'v')
		time.sleep(3)

		# connectionsearch = driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[1]/main/div/form/div/div["
		#                                                 "1]/div[2]/div[4]/div/div[2]/div[2]/div/div/div[1]/div["
		#                                                 "2]/input")
		# connectionsearch.send_keys(row['Connection Name(Output-Hive)'])
		# time.sleep(5)
		# action = ActionChains(driver)
		# source = driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[1]/main/div/form/div/div[1]/div["
		#                                       "2]/div[4]/div/div[2]/div[2]/div/div/div[1]/div[3]/div")
		# action.double_click(source).perform()
		# time.sleep(3)
		driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[1]/main/div/form/div/div["
		                             "1]/header/div/div/div/div/button[2]").click()
		time.sleep(3)

main()