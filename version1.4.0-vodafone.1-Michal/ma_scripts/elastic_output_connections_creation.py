import pandas as pd
from selenium import webdriver
import time
import pyperclip
from selenium.webdriver.common.keys import Keys
import os
from webdriver_manager.chrome import ChromeDriverManager

debug = False

driver = webdriver.Chrome(ChromeDriverManager().install())

COUNTRY = "VFHU"
VENDOR = "Huawei"
TECH = "2G"
config_filename = "VFHU_Huawei_2G.xlsx"

URL_ADDRESS = "http://vodafone-michal.vodafone.cardinality.pl/analytics/"
CONNECTION_TYPE = "elastic"
OUTPUT_IP_ADDRESS = "10.0.0.122"
OUTPUT_IP_PORT = "31921"


def main():
	# reading the config file
	project_url_mapping = dict()
	df = pd.read_excel("data/" + COUNTRY + "/" + VENDOR + "/" + TECH + "/" + config_filename)
	data = df
	df = df.drop_duplicates(subset='Connection Name(Output-Elastic)', keep="first")
	data = data.drop_duplicates(subset='project_name',keep='first')

	driver.get(URL_ADDRESS)
	username_elem = driver.find_element_by_id('username')
	password_elem = driver.find_element_by_id('password')
	username_elem.send_keys('admin')
	password_elem.send_keys('wkswks12')

	driver.find_element_by_tag_name('button').click()
	time.sleep(5)
	df = df.reset_index(drop=True)
	data = data.reset_index(drop=True)

	for index, row in data.iterrows():
		project_name = row['project_name']
		# print(project_name)
		searchbar = driver.find_element_by_css_selector('.MuiInputBase-root.MuiInput-root.jss272.jss278 input')
		searchbar.send_keys(
			project_name)
		time.sleep(1)
		address_link = driver.find_element_by_css_selector(".MuiGrid-root.MuiGrid-item.MuiGrid-grid-md-3 a").get_attribute("href")
		project_url_mapping[row['project_name']] = address_link
		searchbar.click()
		searchbar.send_keys(Keys.CONTROL, 'a')
		searchbar.send_keys(Keys.DELETE)
	# print(project_url_mapping)

	# if your code stops working in between, look at the terminal. What ever is the last index put it here and run again
	# last_index = 135
	# df = df[last_index:]

	for index, row in df.iterrows():
		print("Index: {0} \t Output Connection Name: {1}".format(index, row['Connection Name(Output-Elastic)']))
		connection_name = row['Connection Name(Output-Elastic)']
		project_name = row['project_name']

		URL = project_url_mapping.get(project_name)
		# print(URL)
		driver.get(URL)
		time.sleep(2)
		# create button
		create = driver.find_element_by_id("1").click()
		time.sleep(1)
		menu_list = driver.find_elements_by_tag_name("li")
		# print(menu_list)
		for item in menu_list:
			# print(items.text)
			if item.text == 'Connection':
				item.click()
				time.sleep(2)
				break

		driver.find_element_by_id('name').send_keys(connection_name)
		driver.find_element_by_id('type').click()
		time.sleep(3)
		items = driver.find_elements_by_class_name('MuiMenuItem-gutters')
		for item in items:
			# print(item.text)
			if item.text.lower() == 'elasticsearch':
				item.click()
				time.sleep(1)
				break
		driver.find_element_by_id('server').send_keys(OUTPUT_IP_ADDRESS)
		driver.find_element_by_id('port').send_keys(OUTPUT_IP_PORT)
		pyperclip.copy(row["Elastic Index"])
		driver.find_element_by_id('index').send_keys(Keys.CONTROL, 'v')
		# time.sleep(2)
		for i in ['schema']:
			driver.find_element_by_id('addConfig1').click()
			time.sleep(1)
			items = driver.find_elements_by_class_name('MuiMenuItem-gutters')
			for item in items:
				# print(item.text)
				if item.text.lower() == i:
					item.click()
					time.sleep(1)
					break
		time.sleep(1)
		if not debug:
			driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[1]/main/div/div["
			                             "3]/form/header/div/div/div/div/button[2]").click()


main()
