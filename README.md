 
# Setting Up the Project
- Install Python 3 and more.
- Clone this project with `git clone https://SehraChirag@bitbucket.org/SehraChirag/automating-ae.git`
- Setup a virtual environment named `venv` by going to `Python Interpreter` in `File > Settings`.
- Activate virtual environment and run `pip install -r requirements.txt`.
 
## Folder Structure
```
AE-Automation
|___requirements.txt
|___data
|    |___VFIT
|        |___Huawei
|            |___2G
|                |__logs
|                |    |___elastic_connection_VFIT_Huawei_2G.csv
|                |    |___models_VFIT_Huawei_2G.csv
|                |__q1
|                |    |___abc_q1.txt
|                |    |___xyz_q1.txt
|                |__q2
|                |    |___abc_q2.txt
|                |    |___xyz_q2.txt
|                |__models
|                    |__abc_models.csv
|                    |__xyz_models.csv
|___IP_Instance
|    |___ma_scripts
|    |    |___elastic_output_connections_creation.py
|    |    |___ericsson_models_creation_v10.py
|    |    |___ericsson_models_creation_v62.py
|    |    |___hive_output_connections_creation.py
|    |    |___huawei_models_creation.py
|    |    |___input_connections_creation.py
|    |    |___nokia_models_creation.py
|    |    |___q1_queries_creation.py
|    |    |___q2_queries_creation.py
|    |___vc_scripts
|         |___elastic_output_connections.py
|         |___hive_output_connections_.py
|         |___input_connections.py
|         |___nokia_models_creation.py
|         |___q1_queries_creation.py
|___vodafone-main-instance
     |___ma_scripts
     |    |___elastic_output_connections_creation.py
     |    |___ericsson_models_creation_v10.py
     |    |___ericsson_models_creation_v62.py
     |    |___hive_output_connections_creation.py
     |    |___huawei_models_creation.py
     |    |___input_connections_creation.py
     |    |___nokia_models_creation.py
     |    |___q1_queries_creation.py
     |    |___q2_queries_creation.py
     |___vc_scripts
          |___elastic_output_connections.py
          |___hive_output_connections_.py
          |___input_connections.py
          |___nokia_models_creation.py
          |___q1_queries_creation.py

```